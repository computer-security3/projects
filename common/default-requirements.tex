\newcommand{\clientserver}{client\,/\,server\xspace}

\newcommand{\defaultintro}{This document details the features required for the computer project to implement for the security course. You will find here details about the requirements of your applications, along with the constraints to respect and the submission procedure. Deadlines are given in Section~\ref{sec:modalities}.}

\newcommand{\freedom}{
A lot of freedom is left to your discretion regarding the security policy and actual data storage. Considering the main aspect of this project is security, appropriate techniques have to be used, whether they have been covered in class or not.

Hence, although you are free to choose protocols and languages that you find appropriate, you are responsible for these choices. That is, should you favour some technique over another, and if it figures the choice you made is not relevant regarding security, you will be penalised. Moreover, between to equivalently secure solutions, you have to favour the most efficient one, even if it complexifies the system.

Finally, the features labelled ``Master note'' and ended by the ``{\LARGE$\blacktriangleleft$}'' symbol only apply to students in a Master's cursus.
}

\newcommand{\defaultcharacteristics}{
The architecture to use for your system is ``\clientserver''. Consequently, there are only two types of actors: a server, and a set of clients driven by users.

From a general point of view, the server allows:
\begin{itemize}
  \item new users to register to the system;
  \item users to log in the system;
  \item authenticated users to perform various tasks, when the context is appropriate.
\end{itemize}

Information described in the next part of the document specifying these features is deliberately high-level: it is your job to detect the key points to secure in your project, and how to do it. For that purpose, you are allowed to deviate from what is written here in order to strengthen security.
}

\newcommand{\untrustedserver}{
The server is a \emph{not} trusted entity. In particular, you do not know its set of public keys. Consequently, you have to provide a mechanism to ``securely'' transfer it and checking its ownership.

Furthermore, the server should not be trusted with \emph{any} kind of sensitive information, such as confidential data and critical cryptographic material. Hence, you are expected to decide what security to implement regarding the transmission of data to the server, as well as regarding the storage of data on the server.
}

\newcommand{\defaultauthentication}{
The client allows users registered in the server to authenticate themselves in order to use the features described above. As with any authenticated system, a user has the following attributes:
\begin{itemize}
  \item some authentication material (passwords, cryptographic keys, and/or whatever you find fitting);
  \item a set of information mandatory for its secure communication with the server, such as keys. You are free to handle the generation of that information when a new client registers in any way you find satisfactory.\\
\end{itemize}
}

\newcommand{\sensitive}{
In this context, a sensitive information is an information that should be only be accessible to those users who are explicitly granted access (see the details under section~\ref{sec:features}).
}

\newcommand{\etoeencryption}{
Furthermore, note that in no way you may consider that the server is uncompromised regarding storage. That is, if the server is compromised\footnote{For instance, if an administrator maliciously updates the server so that he recovers every password sent to it, or steals any cryptographic key stored on the server.}, the confidentiality of any sensitive information stored to the server must never be put in jeopardy.
}

\newcommand{\metadata}{
Note that, for each ``pack of data'' or request sent to the server, an associated set of metadata is also sent to the server. This metadata contain at least
\begin{itemize}
\item the time the actual data or request was sent,
\item the size of the data or request,
\item the privileges needed for the request (if relevant),
\item the tree depth of the required resource (if relevant),
\end{itemize}
and any other metadata you see fit meant to be used by the server for anomaly detection purposes\footnote{A machine learning model is not expected here, since you won't have enough data to train it.}. Note that this metadata cannot in any way depend on the content of actual data or request, which is highly sensitive.
}

\newcommand{\defaultfeatures}{
In each of the following protocols, data exchange must be secured at best (according to the relevance of the implemented protection). The same remark can be applied to the storage of data resulting from an exchange. Obviously, if some files are stored ciphered, when a user downloads them, the system has to decipher the considered files.

The type and level of security to use are left to your discretion. Consequently, it is recommended to implement more measures than those dedicated to integrity, confidentiality and authenticity, such as denial of service, dictionary attacks and injections.

\begin{mastermodality}
It is expected that a robust system of logs records user's activity, and that there is sufficient monitoring against attacks, for instance with a form of input sanitisation and automated log analysis.
\end{mastermodality}
}

\newcommand{\deviateifyouwant}{
Again, note that you are allowed to deviate from the protocols described here, as long as these changes are motivated by security. However, you are responsible for these choices: should you change a protocol by another less secured, you will be penalised. Additionally, out of two equivalently secure solutions to a problem, you have to favour the most efficient one\footnote{Obviously, simplicity is to be favoured over complexity, but if a simpler solution hinders the performances of a system, you have to coplexify your application to make it more efficient.}.

Finally, remember that \emph{only} security is graded here: you won't be penalised if you decide not to follow the guidelines and good practices of web-development (as long as it has no negative impact on security and efficiency).
}

\newcommand{\defaultregistration}{
When a new user wants to register to the server, after the authenticity of the server has been verified, credentials have to be generated for the user. The form of these credentials (passwords, keys, etc.) is left to your discretion.
}

\newcommand{\userscanlog}{
After this step, the user can log in the system, by giving its credentials.
}

\newcommand{\changedevices}{
Any user can, at any point, change his credentials and any piece of information he uses to securely communicate with the server, or store information. Furthermore, it must be possible for users to log in from different devices.
}

\newcommand{\defaultcontacts}{
A user can request to add some other user (a contact) to his list of contacts. If the target contact exists, the server notifies him. The contact then decides to deny or accept the request, in which case he notifies the server. The server then notifies both users that they are now in each other's contact list.
}

\newcommand{\defaultsubmission}{
Projects can be implemented \composition, and submitted with the help of a gitlab\footnote{The only two allowed instances are \url{gitlab.com} and \url{git.esi-bru.be}.} repository\footnote{Create the repository yourself, add your teachers (\teachers) as maintainers.}. For that purpose, send us an email on \deadlinesubscription\ at the latest with the ssh URL\footnote{A gitlab ssh URL looks like \texttt{git@instance.com:username/projectname.git}~.} to your repository\footnote{The automatic email notification is \emph{not} enough.}, and the name and matricule of your group members.

You have to submit your work on \deadline\ at the latest. The minimal requirements for submitted projects are as follows:
\begin{itemize}
  \item projects have to be submitted on time,
  \item projects have to provide a \texttt{README} file
    \begin{itemize}
      \item mentioning the name and matricule of your group members,
      \item explaining how to build\footnote{It is expected that your script installs missing dependencies in addition to compiling your code.} your project on a x64 ubuntu 22.04 distribution or a x64 Windows 10 machine (we recommend here to either provide a Makefile, or a shell script to install missing dependencies, compile the project and run relevant scripts),
      \item explaining how to use your project (for example, ``to launch the project, type the following command in a shell'').
    \end{itemize}
\end{itemize}

Projects failing to meet these requirements will not be graded (that is, they will get 0/20). In particular, projects that do not compile according to your \emph{exact} instructions will not be graded. Furthermore, note that we shall in \emph{no way} build or run your projects in an IDE.

As usual, you must provide a modular code, easy to maintain, etc. Moreover,
\begin{itemize}
  \item any submitted code must be duly documented and provide needed configuration files in order to produce the developer documentation.
  \item \emph{only} security features are graded for this project.
\end{itemize}
}

\newcommand{\report}{
Furthermore, any submission must include a report under PDF format detailing your choices regarding security. You are strongly advised to follow the guidelines presented by H.~Mélot~\cite{redacSci} for your redaction, and to answer all questions listed in the check-list attached to this document.
}