\documentclass[a4paper,11pt]{article}

%\usepackage[showframe]{geometry} %use this if you want to check margins
\usepackage{geometry}
\geometry{centering,total={160mm,250mm},includeheadfoot}

\newcommand\thedate{June 2023}
%auxiliairy files
\input{header.tex} %usepackage and configurations
\input{cmds.tex} %user defined commands

%title
\newcommand{\doctitle}{Security}
\newcommand{\docsubtitle}{Computer project}
%\newcommand{\tdyear}{2017 - 2018}
\author{R. Absil}

\newcommand{\deadlinesubscription}{28 May at 23h59\xspace}
\newcommand{\deadline}{11 June at 23h59\xspace}

%language input
\lstset{language=c++,
  morekeywords={constexpr,nullptr}}

%language={[x86masm]Assembler}
%language=c++, morekeywords={constexpr,nullptr}
%language=Java

\newcommand{\clientserver}{client\,/\,server\xspace}
\usepackage{todonotes}

\begin{document}
\pagestyle{fancy} %displays custom headers and footers

\maketitle

This document details the features required for the computer project to implement for the security course. You will find here details about the requirements of your applications, along with the constraints to respect and the submission procedure. The deadline is set on \deadline.
\tableofcontents

\section{Introduction}

The goal of this project is to implement a \emph{secure} \clientserver system handling patient's medical records, such as the ones available in hospitals. A lot of freedom is left to your discretion regarding the security policy and actual data storage. Considering the main aspect of this project is security, appropriate techniques have to be used, whether they have been covered in class or not.

Hence, although you are free to choose protocols and languages that you find appropriate, you are responsible for these choices. That is, should you favour some technique over another, and if it figures the choice you made is not relevant regarding security, you will be penalised.

\section{System Characteristics}

The architecture to use for your system is ``\clientserver''. Consequently, there are only two types of actors: a server, and a set of clients driven by users.

From a general point of view, the server allows:
\begin{itemize}
  \item new users to register to the system;
  \item users to log in the system;
  \item authenticated users to edit medical records and ``share'' them when the context is appropriate.
\end{itemize}

Information described in the next part of the document specifying these features is deliberately high-level: it is your job to detect the key points to secure in your project, and how to do it. For that purpose, you are allowed to deviate from what is written here in order to strengthen security.

%From a network perspective, your project is only required to work behind a NAT: you can consider that the clients and server are on the same (virtual) local network. Note that you cannot make security hypotheses regarding this topology\footnote{For instance, you cannot state ``I'm in a local network, so I'm in a trusted network.''}.

\subsection*{The server}

The server is a \emph{not} trusted entity: you do not know its set of public keys. Consequently, you have to provide a mechanism to ``securely'' transfer it and checking its ownership.

On the other hand, it is your job to decide what security to implement regarding the transmission of data to the server, as well as regarding the storage of data on the server.

\subsection*{Clients and users}

The client allows users registered in the server to authenticate themselves in order to use the features described above. Consequently, any user has the following attributes:
\begin{itemize}
  \item some authentication material (passwords, cryptographic keys, and/or whatever you find fitting);
  \item a set of information mandatory for its secure communication with the server, such as keys. You are free to handle the generation of that information when a new client registers in any way you find satisfactory.\\
\end{itemize}

Furthermore, there are three types of users:
\begin{itemize}
\item administrators,
\item doctors,
\item patients.
\end{itemize}
For simplicity reasons, any user can be of one type only (so, there is no administrator-doctor, no doctor-patient, etc.). The rights that these users have are described in Section~\ref{sec:features}.

Furthermore, patients have a list of associated doctors (the specialists that actually take care of them). We say that these doctors have been \emph{appointed} to the patient. Obviously, a doctor can be appointed to multiple patients.

\subsection*{Medical records}

By medical record, we mean a directory containing various files\footnote{For simplicity reasons, no particular structure has to be enforced in a medical records.}. Note that, both the content of the files and their names are considered sensitive information. Furthermore, note that in no way you may consider that the server is uncompromised regarding file storage. That is, if the server is compromised\footnote{For instance, if an administrator maliciously updates the server so that he recovers every password sent to it.}, the confidentiality of the files stored to the server must never be put in jeopardy.

\section{Features}\label{sec:features}

In each of the following protocols, data exchange must be secured at best (according to the relevance of the protection provided). The same remark can be applied to the storage of data resulting from an exchange. Obviously, if files are stored ciphered, when a user downloads them, the system has to decipher the considered files.

The type and level of security to use are left to your discretion. Consequently, it is recommended to implement more measures than those dedicated to integrity, confidentiality and authenticity, such as denial of service, dictionary attacks and injections. Also remember that a proper system of logs has to be implemented, that is, among others, logs resistant to forgery, especially by administrators.

Again, note that you are allowed to deviate from the protocols described here, as long as these changes are motivated by security. However, you are responsible for these choices: should you change a protocol by another less secured, you will be penalised.

\subsection*{User registration, authentification and revocation}

When a new user wants to register to the server, after the authenticity of the server has been verified, credentials have to be generated for the user. The form of these credentials (passwords, keys, etc.) is left to your discretion.

Note that the registration of an administrator or a doctor can only be completed with the explicit approval of an administrator. Administrators can also delete users.

%A user that has made a registration request is not able to log in until a system administrator has manually accepted the request.

After this step, the user can log in the system, by giving its credentials. %It is expected that the system provides a 2-factor authentification protocol to log in, for instance with the help of a software token.

%An administrator can also deactivate or delete a user.
Note that any user can, at any point, change his credentials and any piece of information he uses to securely communicate with the server, or store information. Furthermore, users can lon in from different devices.

\subsection*{Adding / deleting a doctor}

A patient can add or remove a doctor to his list of appointed doctors. Note that this action can be initiated by the considered doctor himself, but then require explicit approval from the patient.

\subsection*{Consulting a medical record}

Patients can consult their own medical records at any time, as well as doctors who are appointed to them. Note that no one else has access to these medical records.

\subsection*{Uploading, editing and deleting files from the server}

A patient can submit a file to the server to be part of his medical record, which will be uploaded from its computer. In the same way, a patient can overwrite the content of a file from his medical record, or delete it permanently.

Note that these actions can be initiated by a doctor appointed to the patient (the file is then uploaded from the doctor's computer), but then require explicit approval from the patient.


%\subsection*{Group files}
%
%A set of users can decide, together, to upload a file to the server. By using the principles of threshold cryptography, the file cannot be recovered / deleted unless a sufficient amount of users cooperate to perform this operation.

\section{Submission}

Projects have to be implemented in pairs (groups of 2 students), and submitted with the help of a git\footnote{The only instances accepted are \url{https://gitlab.com} and \url{https://git.esi-bru.be}.} repository\footnote{Create the repository yourself, add your teacher as maintainer.}. For that purpose, send an email\footnote{The automatic notification is \emph{not enough}.} to your teacher on \deadlinesubscription at the latest with the SSH URL\footnote{A git SSH URL looks like \texttt{git@git.esi-bru.be:username/projectname}.} to your repository, and the name and matricule of your group members\footnote{I love footnotes.}.

You have to submit your work on \deadline at the latest. The minimal requirements for submitted projects are as follows:
\begin{itemize}
\item projects have to be submitted on time\footnote{I retrieve your projects \emph{on time} with the command \texttt{git pull origin main}. In no way shall I grade or even look at another branch.},
\item projects have to provide a readme file
	\begin{itemize}
	\item mentioning the name and matricule of your group members,
	\item explaining how to build your project\footnote{Projects that do not compile will not be graded.} (we recommend here to either provide a makefile, or a shell script to install missing dependencies, compile the project and run relevant scripts),
	\item explaining how to use your project (for example, ``to run the cryptanalysis of Vernam, run the following command in a shell'').
	\end{itemize}
\end{itemize}

Projects failing to meet these requirements will not be graded (that is, they will get 0/20). In particular, projects that do not compile according to your \emph{exact} instructions will not be graded. Furthermore, note that we shall in \emph{no way} build or run your projects in an IDE.

As usual, you must provide a modular code, easy to maintain, etc. Moreover,
\begin{itemize}
  \item any submitted code must be duly documented and provide needed configuration files in order to produce the developer documentation.
  \item \emph{only} security features are graded for this project,
  \item any submission must include a report under PDF format detailing your choices regarding security. You are advised to follow the guidelines presented by H. Mélot~\cite{redacSci} for your redaction, and to answer to all questions listed in the check-list attached to this document.
\end{itemize}

%input bibliography
\bibliographystyle{abbrv}
\bibliography{biblio}

\end{document}
